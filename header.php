<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <div class="m-0 text-app_black text-base font-[Inter] px-2 xl:px-0">
        <header class="h-16 border-b">
            <div class="max-w-7xl mx-auto h-full flex items-center justify-between sm:justify-start">
                <div class="w-8 h-8 sm:hidden">
                    <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" fill="none">
                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <path fill="#000000" fill-rule="evenodd" d="M19 4a1 1 0 01-1 1H2a1 1 0 010-2h16a1 1 0 011 1zm0 6a1 1 0 01-1 1H2a1 1 0 110-2h16a1 1 0 011 1zm-1 7a1 1 0 100-2H2a1 1 0 100 2h16z"></path>
                        </g>
                    </svg>
                </div>
                <div class="w-10 h-10">
                    <?php the_custom_logo(); ?>
                </div>
                <div class="w-full hidden sm:flex items-center justify-center gap-4 uppercase font-semibold select-none">
                    <div class="duration-300 hover:cursor-pointer hover:text-green-700 flex items-center justify-center">
                        <div class="whitespace-nowrap">menu 1</div>
                        <div class="w-4 h-4"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                <g id="SVGRepo_iconCarrier">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.29289 8.29289C4.68342 7.90237 5.31658 7.90237 5.70711 8.29289L12 14.5858L18.2929 8.29289C18.6834 7.90237 19.3166 7.90237 19.7071 8.29289C20.0976 8.68342 20.0976 9.31658 19.7071 9.70711L12.7071 16.7071C12.3166 17.0976 11.6834 17.0976 11.2929 16.7071L4.29289 9.70711C3.90237 9.31658 3.90237 8.68342 4.29289 8.29289Z" fill="#000000"></path>
                                </g>
                            </svg></div>
                    </div>
                    <div class="duration-300 hover:cursor-pointer hover:text-green-700">menu 2</div>
                    <div class="duration-300 hover:cursor-pointer hover:text-green-700">menu 3</div>
                    <div class="duration-300 hover:cursor-pointer hover:text-green-700">menu 4</div>
                    <div class="duration-300 hover:cursor-pointer hover:text-green-700">menu 5</div>
                    <div class="group relative cursor-pointer py-2">
                        <div class="flex items-center justify-between space-x-5 bg-white px-4">
                            <a class="menu-hover my-2 py-2 text-base font-medium text-black lg:mx-4" onClick="">
                                Our Products
                            </a>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                </svg>
                            </span>
                        </div>
                        <div class="invisible absolute z-50 flex w-full flex-col bg-gray-100 py-1 px-4 text-gray-800 shadow-xl group-hover:visible" onClick="">
                            <a class="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product One</a>

                            <a class="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Two</a>
                            <a class="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Three
                            </a>
                            <a class="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Four
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </header>
        <div class="max-w-7xl mx-auto">