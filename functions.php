<?php


if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.1');
}

function new_theme_assets()
{
    wp_enqueue_style('new-theme-styles', get_template_directory_uri() . '/style.css', false, _S_VERSION, 'all');
}
add_action('wp_enqueue_scripts', 'new_theme_assets');


function new_theme_setup()
{
    //   Add support for core custom logo.
    add_theme_support(
        'custom-logo',
        array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        )
    );
}
add_action('after_setup_theme', 'new_theme_setup');


add_filter('get_custom_logo', 'add_custom_logo');
function add_custom_logo()
{
    $custom_logo_id = get_theme_mod('custom_logo');
    $html           = sprintf(
        '<a href="%1$s" class="object-cover w-full h-full" rel="home" itemprop="url">%2$s</a>',
        esc_url(home_url('/')),
        wp_get_attachment_image(
            $custom_logo_id,
            'full',
            false,
            array(
                'class'    => 'custom-logo svg',
                'itemprop' => 'logo',
            )
        )
    );
    return $html;
}
