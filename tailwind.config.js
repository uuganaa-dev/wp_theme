/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.php"],
  theme: {
    extend: {
      colors: {
        app_black: "#161618",
      },
    },
  },
  plugins: [],
};
